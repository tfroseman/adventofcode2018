package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	double_match, tripple_match := 0, 0
	dat, err := ioutil.ReadFile("input.txt")
	check(err)

	// Split string into []string on new lines
	explodedString := strings.SplitAfter(string(dat), "\n")

	// Strip the new line
	for i := 0; i < len(explodedString); i++ {
		explodedString[i] = strings.TrimSuffix(explodedString[i], "\n")
	}

	matchmap := make(map[uint8]int)
	no_double_duep, no_tripple_duep := true, true

	for i := 0; i < len(explodedString); i++{

		fmt.Println("Testing: ", explodedString[i])

		for j := 0; j < len(explodedString[i]); j++ {
			matchmap[explodedString[i][j]] += 1
		}

		for _, v := range matchmap {
			if(v == 2 && no_double_duep){
				no_double_duep = false
				double_match += 1
			}
			if(v == 3 && no_tripple_duep){
				no_tripple_duep = false
				tripple_match += 1
			}
		}

		fmt.Println(matchmap , "\n\n")

		//Reset duplicate cathing
		no_tripple_duep, no_double_duep = true, true

		//Clear map
		for k := range matchmap {
			delete(matchmap, k)
		}
	}

	fmt.Println("Doubles are: ", double_match, "Triples are: ", tripple_match)
	fmt.Println("Checksum is: ", double_match*tripple_match)
}
