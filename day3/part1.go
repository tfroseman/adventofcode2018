package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

const (
	maxInt = int(^uint(0) >> 1)
	minInt = -maxInt - 1
)

type Point struct {
	pos_x int
	pos_y int
}

type Rectangle struct {
	width int
	height int
}

/**
 #1 | @ | 1,3: | 4x4
  0 | 1 |  2   |  3
 */

func main() {
	area_of_overlap := 0

	overlapMap := make(map[Point]int)
	dat, err := ioutil.ReadFile("input.txt")
	check(err)

	// Split string into []string on new lines, not keeping new line
	explodedString := strings.Split(string(dat), "\n")

	for i:=0; i < len(explodedString); i++ {
		// Split and strip string at spaces
		split_spaces := strings.Split(explodedString[i], " ")
		fmt.Println(split_spaces)

		//Split the point location at a ,
		split_number := strings.Split(split_spaces[2], ",")
		//fmt.Println(split_number)
		
		// Left most number is clean after the first split
		left_number, _ := strconv.Atoi(split_number[0])
		
		//Right number will still contain a :
		right_number, _ := strconv.Atoi(strings.TrimSuffix(split_number[1], ":"))
		
		// 
		point := Point{left_number, right_number}
		//fmt.Println(point)
		
		//
		// Start working out the size of the rectangle
		//
		rectangle_size := strings.Split(split_spaces[3], "x")
		width, _ := strconv.Atoi(rectangle_size[0])
		height, _:= strconv.Atoi(rectangle_size[1])


		for j := point.pos_y; j<point.pos_y+height; j++{
			for k := point.pos_x; k < point.pos_x+width; k++ {
				insertPoint := Point{k, j}
				overlapMap[insertPoint] += 1

				if overlapMap[insertPoint] > 1 {

				}
				//fmt.Println("Inserting: ", insertPoint)
			}
		}

		//fmt.Println(overlapMap)



	}
	//max_overlap :=maxMapValue(overlapMap)
	for _,n := range overlapMap {
		if n > 1 {
			area_of_overlap +=1
		}
	}
	fmt.Println("Area of overlap: ", area_of_overlap)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
