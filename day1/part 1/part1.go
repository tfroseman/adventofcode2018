package main

import (
	"adventofcode2018/utils"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
	"time"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	defer utils.TimeTrack(time.Now())

	dat, err := ioutil.ReadFile("../input.txt")
	check(err)

	resulting_frequency := 0
    explodedString := strings.Split(string(dat), "\n")

	for cursor := range explodedString {

		value, err := strconv.Atoi( (explodedString[cursor]) )

		check(err)

		resulting_frequency = resulting_frequency + value
	}

	fmt.Println(resulting_frequency)
}
