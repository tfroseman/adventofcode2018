package main

import (
	"adventofcode2018/utils"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
	"time"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}


func main() {
	defer utils.TimeTrack(time.Now())

	resulting_frequency := 0
	frequency_map := make(map[int]int)

	dat, err := ioutil.ReadFile("input.txt")
	check(err)

	explodedString := strings.Split(string(dat), "\n")

	for(true) {

		for cursor := range explodedString {

			value, err := strconv.Atoi( (explodedString[cursor]) )
			check(err)

			resulting_frequency = resulting_frequency + value

			frequency_map[resulting_frequency] += 1

			if (frequency_map[resulting_frequency] == 2) {
				fmt.Println(resulting_frequency)
				return
			}
		}
	}
}
